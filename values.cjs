function values(toGetValues) {
    let valueReturn = [];

    if (typeof toGetValues === 'object') {
        for (let value in toGetValues) {
            if (typeof toGetValues[value] != 'function') {
                valueReturn.push(toGetValues[value]);
            } else {
                return [];
            }
        }
        return valueReturn;
    } else {
        return [];
    }
    
}

module.exports = values;