function mapObject(testObject, cb) {

    if (typeof testObject === 'object') {
        for (let propertyMap in testObject) {
            testObject[propertyMap] = cb(testObject[propertyMap], propertyMap)
        }
        return testObject;
    } else {
        return [];
    }
}

module.exports = mapObject;