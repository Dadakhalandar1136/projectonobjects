function keys(testObject) {
    let toStoreKeys = [];
    
    if (typeof testObject === 'object') {
        for (let key in testObject) {
            toStoreKeys.push(`${key}`);
        }
    } else {
        return [];
    }
    return toStoreKeys;
}

module.exports = keys;