function invert(getInvert) {
    let toInvert = {};

    if (typeof getInvert === 'object') {
        for (let indOfInvert in getInvert) {
            toInvert[getInvert[indOfInvert]] = indOfInvert;
        }
        return toInvert;
    } else {
        return [];
    }
}

module.exports = invert;