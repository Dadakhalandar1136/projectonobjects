let mapObject = require('../mapObject.cjs');
const testObject = { 
    name: 'Bruce Wayne', 
    age: 36, 
    location: 'Gotham' 
};

let cbFunction = function(value, item) {
    return value + item;
} 

let returnResult = mapObject(testObject, cbFunction);
console.log(returnResult);