function pairs(pairEntries) {
    let retPairs = [];

    if (typeof pairEntries == 'object') {
        for (let getPair in pairEntries) {
            retPairs.push([getPair, pairEntries[getPair]]);
        }
        return retPairs;
    } else {
        return [];
    }
}

module.exports = pairs;