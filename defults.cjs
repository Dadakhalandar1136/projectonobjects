function defults(testObject, defultProps) {

    if (typeof testObject === 'object') {
        for (let toDefult in defultProps) {
            if (testObject[toDefult] == undefined) {
                testObject[toDefult] = defultProps[toDefult];  
            }
        }
        return testObject;
    } else {
        return [];
    }
}

module.exports = defults;